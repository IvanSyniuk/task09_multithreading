package com.model;

public class PingPong implements Runnable {
    private static String pingOrPong = "ping";
  static Object object = new Object();



    public void pingOrPong() {
        if (pingOrPong.equals("ping")) {
            pingOrPong = "pong";
        } else
            pingOrPong = "ping";
    }

    public void run() {
        try {
            for (int i = 0; i < 5; i++) {
                synchronized (object) {
                    System.out.println(pingOrPong);
                    pingOrPong();
                    object.notify();
                    object.wait();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
