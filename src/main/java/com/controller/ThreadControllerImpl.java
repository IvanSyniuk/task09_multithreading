package com.controller;

import com.model.PingPong;

public class ThreadControllerImpl implements ThreadContoller {
    public void pingPong() {
        PingPong p1 = new PingPong();
        PingPong p2 = new PingPong();
        Thread t1 = new Thread(p1);
        Thread t2 = new Thread(p2);
        t1.start();
        t2.start();
    }
}
